package seamcarving;

import java.util.*;

public class SeamCarving {    
    //https://en.wikipedia.org/wiki/Seam_carving
    private int calcLowDisruptionMeasure(List<List<Integer>> d) { 
        int length, best, index = 0;
        List<Integer> costs, solns, t;
        List<List<Integer>> costMatrix = new ArrayList<>(), solnMatrix = new ArrayList<>();
        System.out.println("Apskaičiuotos energijos: ");
        for(int eilute = 0; eilute < d.size(); eilute++) { // Eilutes             
            costs = new ArrayList<>();
            solns = new ArrayList<>();
            length = d.get(eilute).size();
            if (eilute == 0) { // Virsutine eilute / uzpildome masyva skaiciais, kad veliau galetume atsekti "seam'a" pagal indeksus be problemu
                costs = d.get(eilute);    
                for(int i=0; i < length; i++)
                    solns.add(-1);   
            } else { // Visada praleidziame pirma eilute "masyvo virsu"
                t = d.get(eilute - 1);
                for(int k = 0; k < length; k++) { //stulpeliai (3 galimi variantai -> Kaire, Vidurys arba Desine t.y GRETIMI SKAICIAI)
                    best = Integer.MAX_VALUE;
                    if (k > 0) {
                        best = t.get(k - 1);
                        index = k - 1;
                    }
                    if (t.get(k) < best) {
                        best = t.get(k);
                        index = k;
                    }
                    if (k < length - 1 && t.get(k + 1) < best) {
                        best = t.get(k + 1);
                        index = k + 1;
                    }
                    if (eilute > 1) {
                        for(int e = 0; e < eilute - 1; e++) {
                            best += d.get(e).get(solnMatrix.get(e + 1).get(index));
                        }
                    }
                    
                    //Talpiname "geriausia" t.y arciausia skaiciu.             
                    costs.add(d.get(eilute).get(k) + best);
                    solns.add(index);
                }
            }  
            System.out.println(costs); 
            costMatrix.add(costs); // Suskaiciuotos "seam'u" "energijos"
            solnMatrix.add(solns); // Sutalpinami "seam'o" indeksai
        }
        //Nunulinamas rezultatu isvedimo masyvas
        List<List<Integer>> pathMatrix = new ArrayList<>();
        for(int eil = 0; eil < d.size(); eil++) {
            List<Integer> p = new ArrayList<>();
            for(int i=0; i < d.get(eil).size(); i++)
                p.add(0);            
            pathMatrix.add(p);    
        } 
        
        //Sudarome "Seam'us"
        List<Integer> botMatrix = costMatrix.get(costMatrix.size() - 1);
        best = Collections.min(botMatrix);
        index = botMatrix.indexOf(best);
//        int minSum = 0;
        for(int i = d.size() - 1; i >= 0; i--) {
//            minSum += costMatrix.get(i).get(index);
            pathMatrix.get(i).set(index, 1);
            index = solnMatrix.get(i).get(index);
        }
//        System.out.println("Mažiausio trikdžio siūlo suma: " + minSum);
        
        //Isvedame rezultatus
        System.out.println("Rezultatai: ");
        for(List<Integer> e : pathMatrix) {
            System.out.println(e);
        }
        
        //Grazinamas maziausias "seam'as"
        return best;
    }
    
    public static void main(String[] args) {
        Random rand = new Random();
        rand.setSeed(System.currentTimeMillis());
        
        Scanner scan = new Scanner(System.in);
        System.out.println("Eilučių skaičius 'm' ");
        int m = scan.nextInt();
        System.out.println("Stulpeliu skaičius 'n'");
        int n = scan.nextInt();
//        int m = Integer.parseInt();
//        int n = Integer.parseInt(System.console().readLine());

        List<List<Integer>> d = new ArrayList<List<Integer>>() {{
            for (int k = 0; k < m; k++) {
                List<Integer> sk = new ArrayList<>();
                for (int i = 0; i < n; i++) {
                    sk.add(rand.nextInt(100));
                }            
                add(sk);
            }
//            add(new ArrayList<>(Arrays.asList(1,4,3,5,2)));
//            add(new ArrayList<>(Arrays.asList(3,2,5,2,3)));
//            add(new ArrayList<>(Arrays.asList(5,2,4,2,1)));
            
//            add(new ArrayList<>(Arrays.asList(3,4,5)));
//            add(new ArrayList<>(Arrays.asList(7,1,5)));
//            add(new ArrayList<>(Arrays.asList(1,9,9)));
        }};
        
        System.out.println("Duomenys: ");
        for(List<Integer> e : d) {
            System.out.println(e);
        }
        long start = System.nanoTime();
        System.out.println("Mažiausio trikdžio siūlas: " + new SeamCarving().calcLowDisruptionMeasure(d));
        long time = System.nanoTime() - start;
        System.out.format("Greitaveika: %f\n", time * 1e-9);
       
    }
}